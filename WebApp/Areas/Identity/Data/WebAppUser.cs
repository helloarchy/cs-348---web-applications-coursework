﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace WebApp.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the WebAppUser class
    public class WebAppUser : IdentityUser
    {
        // User can delete/download this data
        [PersonalData] public override string UserName { get; set; }
        [PersonalData] public string FirstName { get; set; }
        [PersonalData] public string LastName { get; set; }

        // User cannot delete/download
        public DateTime Created { get; set; }
    }
}